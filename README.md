# Relative-Dates

Relative dates doesn't show the exact date, it shows how old something is. Like a post.

Relative Date cascade to only 2 levels.

**The Dates are showed as the following:**

- Years, months [2 years, 11 months]
- Years, weeks [4 years, 32 weeks]
- Months, weeks
- Months, days
- Weeks, days
- Days, hours
- Hours, minutes
- Minutes, seconds
- Seconds


## Installation

1. Upload the entire `songer-relative-date` folder to your wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. That's it