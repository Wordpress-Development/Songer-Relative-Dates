<?php
/**
* Admin Menu Functions
*
* Various functions relating to the various administration screens
*
* @package	Songer-Relative-Date
*/

/**
* Add meta to plugin details
*
* Add options to plugin meta line
*
* @since	1.2
*
* @param	string  $links	Current links
* @param	string  $file	File in use
* @return   string			Links, now with settings added
*/

function songer_set_plugin_meta( $links, $file ) {

	if ( strpos( $file, 'songer-relative-date.php' ) !== false ) {
		$links = array_merge( $links, array( '<a href="">' . __( 'Support', 'songer-relative-date' ) . '</a>' ) );
		$links = array_merge( $links, array( '<a href="">' . __( 'Donate', 'songer-relative-date' ) . '</a>' ) );
	}

	return $links;
}

add_filter( 'plugin_row_meta', 'songer_set_plugin_meta', 10, 2 );
?>