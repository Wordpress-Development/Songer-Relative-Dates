<?php
/*
Plugin Name: Songer Relative Date
Plugin URI: https://
Description: Relative Formatted Date
Version: 0.0.0
Author: Austin Vern Songer
Author URI: http://
Text Domain: songer-relative-date
Domain Path: /languages
*/

/**
* Relative Date
*
* Main code - include various functions
*
* @package	Songer-Relative-Date
* @since	1.2
*/

define( 'songer_relative_date_version', '1.2.2' );

/**
* Plugin initialisation
*
* Loads the plugin's translated strings and the plugins' JavaScript
*
* @since	1.2
*/

function songer_plugin_init() {

	$language_dir = plugin_basename( dirname( __FILE__ ) ) . '/languages/';

	load_plugin_textdomain( 'songer-relative-date', false, $language_dir );

}

add_action( 'init', 'songer_plugin_init' );

/**
* Main Includes
*
* Include all the plugin's functions
*
* @since	1.2
*/

$functions_dir = WP_PLUGIN_DIR . '/songer-relative-date/includes/';

// Include all the various functions

include_once( $functions_dir . 'songer-generate-relative-date.php' );		// Main code to perform currency conversion

if ( is_admin() ) {

	include_once( $functions_dir . 'songer-admin-config.php' );        	// Assorted admin configuration changes

} else {

	include_once( $functions_dir . 'songer-function-calls.php' );	        // Function calls

}
?>